# Federated Multi-View Probabilistic Principal Component Analysis (Fed-mv-PPCA)


Fed-mv-PPCA provides a Bayesian framework for assimilation of federated multi-view heterogeneous datasets. Here the code used to obtain all results showed in the original paper "A Probabilistic Framework for Modeling the Variability Across Federated Datasets of Heterogeneous Multi-View Observations" (Information Processing in Medical Imaging 2021) is provided, along with the synthetic dataset (the dataset used for the real application, extracted from the Alzheimer’s Disease Neuroimaging Initiative project, is not provided due to confidentiality reasons). Data distribution across centers will be simulated on the local machine.

To run the main:

python main_Fed_MVPPCA.py --data 'SD' --scenario 'IID' 


