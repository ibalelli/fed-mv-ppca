import numpy as np
from csv import writer
import pandas as pd
from sklearn import preprocessing

# Function to append results to csv
def append_list_as_row(file_name, list_of_elem):
    # Open file in append mode
    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        csv_writer = writer(write_obj)
        # Add contents of list as last row in the csv file
        csv_writer.writerow(list_of_elem)

# Function for eigen-decomposition of empirical covariance matrix
def eigen_dec_emp_cov(X_simu_k, D_i, k):

    X_simu_k_mean = sum(X_simu_k)/len(X_simu_k)
    Emp_cov = np.zeros((D_i[k],D_i[k]))
    for l in range(len(X_simu_k)):
        Emp_cov += (X_simu_k[l]-X_simu_k_mean).dot((X_simu_k[l]-X_simu_k_mean).T)
    Emp_cov = Emp_cov/(len(X_simu_k)-1)

    # eigenvalues decomposition and sort
    eigen_values, eigen_vectors = np.linalg.eig(Emp_cov)
    pairs = [(np.abs(eigen_values[i]), eigen_vectors[:, i], np.sign(eigen_values[i])) for i in
             range(len(eigen_values))]
    pairs = sorted(pairs, key=lambda x: x[0], reverse=True)

    eigen_val_sorted = []
    for pair in pairs:
        eigen_val_sorted.append(pair[0] * pair[2])

    eigen_value_sums = sum(eigen_values)
    print('Explained Variance')
    for i, pair in enumerate(pairs):
        print('Eigenvector {}: {}'.format(i, (pair[0] / eigen_value_sums).real))

    return eigen_val_sorted

# Save first columns of t_n_k to csv for plots
def t_n_k(XC, ViewsXC, D_i, CENTERS, K, norm):
    Tk = pd.DataFrame()  # t_n(k) for each center
    for c in CENTERS:
        T_n_c = XC[c].sort_values(XC[c].columns[0])
        ind = 0
        for k in range(K):
            if ViewsXC[c][k] == 1:
                XCK = T_n_c.iloc[:, ind:ind + 2]
                ind += D_i[k]
                XCK.columns = ['tnk_0', 'tnk_1']
                if norm == True:
                    x = XCK.values  #
                    min_max_scaler = preprocessing.MinMaxScaler()  #
                    x_scaled = min_max_scaler.fit_transform(x)  #
                    XCK = pd.DataFrame(x_scaled, index=XCK.index, columns=XCK.columns)
                XCK['Center'] = c + 1
                XCK['View'] = k + 1
                Tk = Tk.append(XCK, ignore_index=True)
    return Tk


# Evaluate difference among estimated local and global parameters
def Diffs(G_params,L_params_new,CENTERS,ViewsXC,K):
    Diff_1 = []
    Diff_2 = []
    Diff_3 = []
    tilde_muk = G_params[0]
    tilde_Wk = G_params[1]
    tilde_Sigma2k = G_params[2]
    for c in CENTERS:
        muk = L_params_new[0][c]
        Wk = L_params_new[1][c]
        Sigma2 = L_params_new[2][c]
        muk_tilde_muk = []
        Wk_tilde_Wk = []
        Sigma2k_tildeSigma2k = []
        for k in range(K):
            if ViewsXC[c][k] == 1:
                muk_tilde_muk.append(np.linalg.norm(muk[k] - tilde_muk[k]))
                Wk_tilde_Wk.append(np.linalg.norm(Wk[k] - tilde_Wk[k]))
                Sigma2k_tildeSigma2k.append(abs(Sigma2[k] - tilde_Sigma2k[k]))
        Diff_1.append(muk_tilde_muk)
        Diff_2.append(Wk_tilde_Wk)
        Diff_3.append(Sigma2k_tildeSigma2k)
    return Diff_1,Diff_2,Diff_3