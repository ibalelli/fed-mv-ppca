import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
plt.style.use('ggplot')
import numpy as np
import os

####################################################################################################

# verify these parameters are the same as in the main!

####################################################################################################
#
# ADNI
# D_i = [7, 41, 41, 41]
# C = 3
# IID
# ViewsXC = [[1 for _ in range(4)] for _ in range(C)]
# K or G/K
# C = 3
# ViewsXC = [[1, 1, 1, 1], [1, 0, 1, 1], [1, 1, 0, 1]]
# C = 6
# ViewsXC = [[1, 1, 1, 1], [1, 1, 1, 1], [1, 0, 1, 1], [1, 0, 1, 1], [1, 1, 0, 1], [1, 1, 0, 1]]

# SD
D_i = [15, 8, 10]
C = 3
# IID
ViewsXC = [[1 for _ in range(3)] for _ in range(C)]
# K or G/K
# C = 3
# ViewsXC = [[1, 1, 1], [1, 0, 1], [1, 1, 0]]
# C = 6
# ViewsXC = [[1, 1, 1], [1, 1, 1], [1, 0, 1], [1, 0, 1], [1, 1, 0], [1, 1, 0]]
####################################################################################################

script_dir = os.path.dirname(__file__)

# add name results directory
####################################################################################################
results_dir = os.path.join(script_dir, 'Fed_MVPPCA_Results_/')
####################################################################################################

Data = pd.read_csv(results_dir + 'Xn_given_tn.csv', index_col=0)
Tksim = pd.read_csv(results_dir + "Tnksim.csv", index_col=0)
Tk = pd.read_csv(results_dir + "Tnk.csv", index_col=0)
results = pd.read_csv(results_dir + 'Centers_results.csv', index_col=0)
Global_res = pd.read_csv(results_dir + 'Master_results.csv', index_col=0)

results['center'] = results['center'].astype('category')
Tk['Center'] = Tk['Center'].astype('category')
Data['Center'] = Data['Center'].astype('category')

C = len(np.unique(Data['Center']))
K = len(np.unique(Tk['View']))

# plots in each center

CMAPS = ['Blues', 'Reds', 'Greys', 'Greens', 'Purples', 'Oranges', 'rainbow']

# plot of x_n|t_n
plt.figure(figsize=(9, 10), dpi=150)
plt.xlabel('$x_n[1]$')
plt.ylabel('$x_n[2]$')
sns.scatterplot(data=Data,
                x='X_n_0', y='X_n_1', alpha=0.7, hue='Center')
plt.legend();
plt.savefig(results_dir + "Xn_given_tn")
plt.clf()
plt.close()

ax = plt.figure(figsize=(9,10))
Legend = []
ind = 0
for c in range(C):
    label = 'Center = ' + str(c+1)
    Legend.append(label)
    x = Data[(Data.Center == (c+1))]['X_n_0'].to_numpy()
    y = Data[(Data.Center == (c+1))]['X_n_1'].to_numpy()
    sns.kdeplot(x, y, cbar=True, cmap = CMAPS[ind])
    ind += 1

leg = ax.legend(labels=Legend)
for c in range(C):
    leg.legendHandles[c].set_color(sns.color_palette(CMAPS[c])[-2])
plt.xlabel('$x_n[1]$')
plt.ylabel('$x_n[2]$')
plt.title('2D Gaussian Kernel density estimation')
plt.savefig(results_dir + "2Ddensity")
plt.clf()
plt.close()

# plot of first components of tnk for each k vs predicted distribution
plt.figure(figsize=(5*K, 10), dpi=150)
palette = sns.cubehelix_palette(C)
for k in range(K):
    num = int('1' + str(K) + str(k + 1))
    plt.subplot(num).set_title('k=%i'  % (k+1))
    x = Tksim[Tksim.View == (k+1)]['T_n_0'].to_numpy()
    y = Tksim[Tksim.View == (k + 1)]['T_n_1'].to_numpy()
    sns.kdeplot(x, y, cmap="Blues", shade=True, shade_lowest=False)

    # Center_num = Tk[Tk.View == (k+1)].Center.unique()
    # Center_col = [palette[i-1] for i in Center_num]

    # sns.scatterplot(data=Tk[Tk.View == (k+1)], x='tnk_0', y='tnk_1', hue='Center', palette=Center_col, alpha=0.7)
    
    sns.scatterplot(data=Tk[Tk.View == (k + 1)], x='tnk_0', y='tnk_1', hue='Center', palette=palette, alpha=0.7)
    plt.xlabel('$t_n(%i)[1]$' % (k+1))
    plt.ylabel('$t_n(%i)[2]$' % (k+1))
plt.savefig(results_dir + "tn(k)priors")
plt.clf()
plt.close()

# plots for comparaison of convergence among centers

# Expected Log Likelihood
plt.figure(figsize=(9, 10), dpi=150)
sns.lineplot(data=results, x='iter', y='E_L_c', hue='center')
plt.title('$E(L_c)$')
plt.savefig(results_dir + "E(L_c)")
plt.clf()
plt.close()

# norm of mu(c)(k)
plt.figure(figsize=(5*K, 10), dpi=150)
for k in range(K):
    num = int('1' + str(K) + str(k + 1))
    name = 'mu_norm' + '_' + str(k + 1)
    plt.subplot(num)
    sns.lineplot(data=results, x='iter', y=name, hue='center')
    plt.title('$\|\|\mu^{(%i)}\|\|$' % (k + 1))
plt.savefig(results_dir + "mu(k)_norm")
plt.clf()
plt.close()

# norm of W(c)(k)
plt.figure(figsize=(5*K, 10), dpi=150)
for k in range(K):
    num = int('1' + str(K) + str(k + 1))
    name = 'W_norm' + '_' + str(k + 1)
    plt.subplot(num)
    sns.lineplot(data=results, x='iter', y=name, hue='center')
    plt.title('$\|\|W^{(%i)}\|\|$' % (k + 1))
plt.savefig(results_dir + "W(k)_norm")
plt.clf()
plt.close()

# sigma2(c)(k)
plt.figure(figsize=(5*K, 10), dpi=150)
for k in range(K):
    num = int('1' + str(K) + str(k + 1))
    name = 'sigma2' + '_' + str(k + 1)
    plt.subplot(num)
    sns.lineplot(data=results, x='iter', y=name, hue='center')
    plt.title('${\sigma^{(%i)}}^2$' % (k + 1))
plt.savefig(results_dir + "sigma(k)2")
plt.clf()
plt.close()

# plots convergence master parameters

# norm of tilde_mu(k)
plt.figure(figsize=(5*K, 10), dpi=150)
for k in range(K):
    num = int('1' + str(K) + str(k + 1))
    name = 'norm_tilde_mu' + '_' + str(k + 1)
    plt.subplot(num)
    sns.lineplot(data=Global_res, x='round', y=name)
    plt.title('$\|\|\widetilde{\mu}^{(%i)}\|\|$' % (k + 1))
plt.savefig(results_dir + "tilde_mu(k)_norm")
plt.clf()
plt.close()

# sigma2_tilde_mu(k)
plt.figure(figsize=(5*K, 10), dpi=150)
for k in range(K):
    num = int('1' + str(K) + str(k + 1))
    name = 'sigma2_tilde_mu' + '_' + str(k + 1)
    plt.subplot(num)
    sns.lineplot(data=Global_res, x='round', y=name)
    plt.title('$\sigma_{\widetilde{\mu}^{(%i)}}^2$' % (k + 1))
plt.savefig(results_dir + "sigma2_tilde_mu(k)")
plt.clf()
plt.close()

# norm of tilde_W(k)
plt.figure(figsize=(5*K, 10), dpi=150)
for k in range(K):
    num = int('1' + str(K) + str(k + 1))
    name = 'norm_tilde_W' + '_' + str(k + 1)
    plt.subplot(num)
    sns.lineplot(data=Global_res, x='round', y=name)
    plt.title('$\|\|\widetilde{W}^{(%i)}\|\|$' % (k + 1))
plt.savefig(results_dir + "tilde_W(k)_norm")
plt.clf()
plt.close()

# sigma2_tilde_W(k)
plt.figure(figsize=(5*K, 10), dpi=150)
for k in range(K):
    num = int('1' + str(K) + str(k + 1))
    name = 'sigma2_tilde_W' + '_' + str(k + 1)
    plt.subplot(num)
    sns.lineplot(data=Global_res, x='round', y=name)
    plt.title('$\sigma_{\widetilde{W}^{(%i)}}^2$' % (k + 1))
plt.savefig(results_dir + "sigma2_tilde_W(k)")
plt.clf()
plt.close()

# tilde_sigma2(k)
plt.figure(figsize=(5*K, 10), dpi=150)
for k in range(K):
    num = int('1' + str(K) + str(k + 1))
    name = 'tilde_sigma2' + '_' + str(k + 1)
    plt.subplot(num)
    sns.lineplot(data=Global_res, x='round', y=name)
    plt.title('$\widetilde{\sigma}^{2^(%i)}$' % (k + 1))
plt.savefig(results_dir + "tilde_sigma2(k)")
plt.clf()
plt.close()

# sigma2_tilde_sigma2(k)
plt.figure(figsize=(5*K, 10), dpi=150)
for k in range(K):
    num = int('1' + str(K) + str(k + 1))
    name = 'sigma2_tilde_sigma2' + '_' + str(k + 1)
    plt.subplot(num)
    sns.lineplot(data=Global_res, x='round', y=name)
    plt.title('$\sigma_{\widetilde{\sigma}^{2^(%i)}}^2$' % (k + 1))
plt.savefig(results_dir + "sigma2_tilde_sigma2(k)")
plt.clf()
plt.close()
