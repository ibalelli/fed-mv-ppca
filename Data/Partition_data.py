import os
import numpy as np
import pandas as pd
from sklearn.model_selection import StratifiedKFold, train_test_split
import sys
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
data_dir = os.path.dirname(__file__)


ADNI_CSV = os.path.join(data_dir, 'data_irene_dx_2g_corrected.csv')
SYNT_CSV = os.path.join(data_dir, 'Synthetic_data_2g.csv')


def load_csv(dataset='SD'):
    if dataset=='SD':
        X = pd.read_csv(SYNT_CSV, index_col=0)
    elif dataset=='ADNI':
        X = pd.read_csv(ADNI_CSV, index_col=0)

    # Remove spaces and lowercase feature names
    X.columns = [col.strip().lower() for col in list(X.columns)]
    X.rename(columns={'dx': 'label'}, inplace=True)
    return X


def split_IID(dataset='SD', N_Centers=3):
    X = load_csv(dataset=dataset)
    if dataset=='SD':
        D_i = [15, 8, 10]
    elif dataset == 'ADNI':
        D_i = [7, 41, 41, 41]
    groups = np.unique(X['label'])  # total groups in dataset

    data = {}
    data['char'] = {'groups': groups, 'views': D_i}

    # stratify +
    skf = StratifiedKFold(n_splits=3, random_state=21, shuffle=True)
    for fold_i, (train_index, test_index) in enumerate(skf.split(X.drop(columns='label'), X.label), 1):
        X_train, X_test = X.drop(columns='label').iloc[train_index, :], X.drop(columns='label').iloc[test_index, :]
        y_train, y_test = X.label.iloc[train_index], X.label.iloc[test_index]
        Test_data = X_test

        # >= 1 train center
        if N_Centers == 1:
            XC = [X_train]
        else:
            yX_train = pd.concat([y_train, X_train], axis=1)
            yX_train.columns.values[0] = 'label'
            yX_train.columns = [col.strip() for col in list(X.columns)]

            N_subj_gr = []  # Number of subjects per group in each center
            for gr in range(len(groups)):
                N_subj_gr.append(int((yX_train.loc[yX_train.label == groups[gr], :].shape[0]) / N_Centers))
            XC = []
            index_gr = []
            X_C_gr = yX_train.loc[yX_train.label == groups[0], :].sample(n=N_subj_gr[0], random_state=21)
            index_gr.append(X_C_gr.index)
            for gr in range(1, len(groups)):
                X_C_gr_g = yX_train.loc[yX_train.label == groups[gr], :].sample(n=N_subj_gr[gr], random_state=21)
                index_gr.append(X_C_gr_g.index)
                X_C_gr = pd.concat([X_C_gr, X_C_gr_g], axis=0)
            XC.append(X_C_gr.drop(columns='label'))
            for c in range(1, N_Centers - 1):
                X_C_gr = yX_train.loc[yX_train.label == groups[0], :].drop(index_gr[0], axis=0).sample(n=N_subj_gr[0],
                                                                                                       random_state=21)
                index_gr[0] = index_gr[0].union(X_C_gr.index)
                for gr in range(1, len(groups)):
                    X_C_gr_g = yX_train.loc[yX_train.label == groups[gr], :].drop(index_gr[gr], axis=0).sample(
                        n=N_subj_gr[gr], random_state=21)
                    index_gr[gr] = index_gr[gr].union(X_C_gr_g.index)
                    X_C_gr = pd.concat([X_C_gr, X_C_gr_g], axis=0)
                XC.append(X_C_gr.drop(columns='label'))
            X_C_gr = yX_train.loc[yX_train.label == groups[0], :].drop(index_gr[0])
            for gr in range(1, len(groups)):
                X_C_gr_g = yX_train.loc[yX_train.label == groups[gr], :].drop(index_gr[gr])
                X_C_gr = pd.concat([X_C_gr, X_C_gr_g], axis=0)
            XC.append(X_C_gr.drop(columns='label'))

        # Get labels from data
        YC = [X.loc[df.index, 'label'] for df in XC]
        y_test = X.loc[Test_data.index, 'label']

        ViewsXC = [[1 for _ in range(len(D_i))] for _ in range(N_Centers)]
        ViewsTest = [1 for _ in range(len(D_i))]

        data[fold_i] = {'train_sets': (XC, YC, ViewsXC), 'test_set': ([Test_data], [y_test], [ViewsTest])}
    return data

def split_G(dataset='SD', N_Centers=3):
    X = load_csv(dataset=dataset)
    if dataset=='SD':
        D_i = [15, 8, 10]
    elif dataset == 'ADNI':
        D_i = [7, 41, 41, 41]
    groups = np.unique(X['label'])  # total groups in dataset

    data = {}
    data['char'] = {'groups': groups, 'views': D_i}
    experiment_i = 1

    X.columns = [col.strip() for col in list(X.columns)]
    groups = np.unique(X['label'])  # total groups in dataset
    G = len(groups)  # total number of groups

    N_subj_gr = []  # Number of subjects per group in each center
    for gr in range(G):
        N_subj_gr.append(int((X.loc[X.label == groups[gr], :].shape[0]) / 3))

    XC_init = []

    # partition group 1
    X_C_G1_1 = X.loc[X.label == groups[0], :].sample(n=N_subj_gr[0], random_state=21)
    index_G1 = X_C_G1_1.index
    X_C_G1_2 = X.loc[X.label == groups[0], :].drop(index_G1)

    # partition group 2
    X_C_G2_1 = X.loc[X.label == groups[1], :].sample(n=N_subj_gr[1], random_state=21)
    index_G2 = X_C_G2_1.index
    X_C_G2_2 = X.loc[X.label == groups[1], :].drop(index_G2)

    # C1
    XC_init.append(pd.concat([X_C_G1_1, X_C_G2_1], axis=0))

    # C2
    XC_init.append(X_C_G1_2)

    # C3
    XC_init.append(X_C_G2_2)

    # stratify
    skf0 = StratifiedKFold(n_splits=3, random_state=21, shuffle=True)
    for fold_i_0, (train_index_0, test_index_0) in enumerate(
            skf0.split(XC_init[0].drop(columns='label'), XC_init[0].label), 1):
        X_train_0, X_test_0 = XC_init[0].drop(columns='label').iloc[train_index_0, :], XC_init[0].drop(
            columns='label').iloc[test_index_0, :]
        y_train_0, y_test_0 = XC_init[0].label.iloc[train_index_0], XC_init[0].label.iloc[test_index_0]

        skf1 = StratifiedKFold(n_splits=3, random_state=21, shuffle=True)
        for fold_i_1, (train_index_1, test_index_1) in enumerate(
                skf1.split(XC_init[1].drop(columns='label'), XC_init[1].label), 1):
            X_train_1, X_test_1 = XC_init[1].drop(columns='label').iloc[train_index_1, :], XC_init[1].drop(
                columns='label').iloc[test_index_1, :]
            y_train_1, y_test_1 = XC_init[1].label.iloc[train_index_1], XC_init[1].label.iloc[test_index_1]

            skf2 = StratifiedKFold(n_splits=3, random_state=21, shuffle=True)
            for fold_i_2, (train_index_2, test_index_2) in enumerate(
                    skf2.split(XC_init[2].drop(columns='label'), XC_init[2].label), 1):
                X_train_2, X_test_2 = XC_init[2].drop(columns='label').iloc[train_index_2, :], XC_init[2].drop(
                    columns='label').iloc[test_index_2, :]
                y_train_2, y_test_2 = XC_init[2].label.iloc[train_index_2], XC_init[2].label.iloc[test_index_2]

                XC = [X_train_0, X_train_1, X_train_2]
                YC = [y_train_0, y_train_1, y_train_2]

                if N_Centers == 6:
                    XC_6 = []
                    Y_train_6 = []
                    for xc in range(len(XC)):
                        X_train_c, X_test_c, y_train_c, y_test_c = train_test_split(XC[xc], YC[xc], test_size=0.5,
                                                                                    random_state=21)
                        XC_6.extend([X_train_c, X_test_c])
                        Y_train_6.extend([y_train_c, y_test_c])
                    XC = XC_6
                    YC = Y_train_6

                Test_data = pd.concat([X_test_0, X_test_1, X_test_2], axis=0)
                Y_test = [y_test_0, y_test_1, y_test_2]
                Label_Test = pd.Series()
                for c_test in range(len(Y_test)):
                    Label_Test = Label_Test.append(Y_test[c_test])

                ViewsXC = [[1 for _ in range(len(D_i))] for _ in range(N_Centers)]
                ViewsTest = [1 for _ in range(len(D_i))]

                data[experiment_i] = {'train_sets': (XC, YC, ViewsXC), 'test_set': ([Test_data], [Label_Test], [ViewsTest])}
                experiment_i += 1
    return data

def split_GK(dataset='SD', N_Centers=3):
    X = load_csv(dataset=dataset)
    if dataset=='SD':
        D_i = [15, 8, 10]
    elif dataset == 'ADNI':
        D_i = [7, 41, 41, 41]
    groups = np.unique(X['label'])  # total groups in dataset

    data = {}
    data['char'] = {'groups': groups, 'views': D_i}
    experiment_i = 1

    X.columns = [col.strip() for col in list(X.columns)]

    groups = np.unique(X['label'])  # total groups in dataset
    G = len(groups)  # total number of groups

    N_subj_gr = []  # Number of subjects per group in each center
    for gr in range(G):
        N_subj_gr.append(int((X.loc[X.label == groups[gr], :].shape[0]) / 3))

    XC_init = []

    # partition group 1
    X_C_G1_1 = X.loc[X.label == groups[0], :].sample(n=N_subj_gr[0], random_state=21)
    index_G1 = X_C_G1_1.index
    X_C_G1_2 = X.loc[X.label == groups[0], :].drop(index_G1)

    # partition group 2
    X_C_G2_1 = X.loc[X.label == groups[1], :].sample(n=N_subj_gr[1], random_state=21)
    index_G2 = X_C_G2_1.index
    X_C_G2_2 = X.loc[X.label == groups[1], :].drop(index_G2)

    # C1
    XC_init.append(pd.concat([X_C_G1_1, X_C_G2_1], axis=0))

    # C2
    XC_init.append(X_C_G1_2)

    # C3
    XC_init.append(X_C_G2_2)

    # C2 without view 2, C3 without view 3
    # save excluded columns in datasets
    X2_V2 = pd.concat([XC_init[1].iloc[:, 0], XC_init[1].iloc[:, D_i[0] + 1:D_i[0] + D_i[1] + 1]], axis=1)
    XC_init[1] = XC_init[1].drop(XC_init[1].columns[list(range(D_i[0] + 1, D_i[0] + D_i[1] + 1))], axis=1)
    X3_V3 = pd.concat([XC_init[2].iloc[:, 0], XC_init[2].iloc[:, D_i[0] + D_i[1] + 1:D_i[0] + D_i[1] + D_i[2] + 1]],
                       axis=1)
    XC_init[2] = XC_init[2].drop(XC_init[2].columns[list(range(D_i[0] + D_i[1] + 1, D_i[0] + D_i[1] + D_i[2] + 1))],
                                 axis=1)

    # stratify
    skf0 = StratifiedKFold(n_splits=3, random_state=21, shuffle=True)
    for fold_i_0, (train_index_0, test_index_0) in enumerate(
            skf0.split(XC_init[0].drop(columns='label'), XC_init[0].label), 1):
        X_train_0, X_test_0 = XC_init[0].drop(columns='label').iloc[train_index_0, :], XC_init[0].drop(
            columns='label').iloc[test_index_0, :]
        y_train_0, y_test_0 = XC_init[0].label.iloc[train_index_0], XC_init[0].label.iloc[test_index_0]

        skf1 = StratifiedKFold(n_splits=3, random_state=21, shuffle=True)
        for fold_i_1, (train_index_1, test_index_1) in enumerate(
                skf1.split(XC_init[1].drop(columns='label'), XC_init[1].label), 1):
            X_train_1, X_test_1 = XC_init[1].drop(columns='label').iloc[train_index_1, :], XC_init[1].drop(
                columns='label').iloc[test_index_1, :]
            y_train_1, y_test_1 = XC_init[1].label.iloc[train_index_1], XC_init[1].label.iloc[test_index_1]

            skf2 = StratifiedKFold(n_splits=3, random_state=21, shuffle=True)
            for fold_i_2, (train_index_2, test_index_2) in enumerate(
                    skf2.split(XC_init[2].drop(columns='label'), XC_init[2].label), 1):
                X_train_2, X_test_2 = XC_init[2].drop(columns='label').iloc[train_index_2, :], XC_init[2].drop(
                    columns='label').iloc[test_index_2, :]
                y_train_2, y_test_2 = XC_init[2].label.iloc[train_index_2], XC_init[2].label.iloc[test_index_2]

                XC = [X_train_0, X_train_1, X_train_2]
                Y_train = [y_train_0, y_train_1, y_train_2]

                if N_Centers == 6:
                    XC_6 = []
                    Y_train_6 = []
                    for xc in range(len(XC)):
                        X_train_c, X_test_c, y_train_c, y_test_c = train_test_split(XC[xc], Y_train[xc], test_size=0.5,
                                                                                    random_state=21)
                        XC_6.extend([X_train_c, X_test_c])
                        Y_train_6.extend([y_train_c, y_test_c])
                    XC = XC_6
                    Y_train = Y_train_6

                Test_data = [X_test_0, X_test_1, X_test_2]
                Y_test = [y_test_0, y_test_1, y_test_2]

                # save absent views to csv for plots
                X2_V2_test = X2_V2.drop(columns='label').iloc[test_index_1, :]
                X3_V3_test = X3_V3.drop(columns='label').iloc[test_index_2, :]

                X_absent_view = [0, X2_V2_test, X3_V3_test]

                ViewsTest = [[1 for _ in range(len(D_i))] for _ in range(3)]
                ViewsTest[1][1] = 0
                ViewsTest[2][2] = 0
                ViewsXC = [[1 for _ in range(len(D_i))] for _ in range(N_Centers)]
                if N_Centers == 3:
                    ViewsXC[1][1] = 0
                    ViewsXC[2][2] = 0
                elif N_Centers == 6:
                    ViewsXC[2][1] = 0
                    ViewsXC[3][1] = 0
                    ViewsXC[4][2] = 0
                    ViewsXC[5][2] = 0

                data[experiment_i] = {'train_sets': (XC, Y_train, ViewsXC), 'test_set': (Test_data, Y_test, ViewsTest, X_absent_view)}
                experiment_i += 1
    return data


def split_K(dataset='SD', N_Centers=3):
    X = load_csv(dataset=dataset)
    if dataset=='SD':
        D_i = [15, 8, 10]
    elif dataset == 'ADNI':
        D_i = [7, 41, 41, 41]
    groups = np.unique(X['label'])  # total groups in dataset

    data = {}
    data['char'] = {'groups': groups, 'views': D_i}
    experiment_i = 1

    X.columns = [col.strip() for col in list(X.columns)]

    groups = np.unique(X['label'])  # total groups in dataset

    # We split all data iid in 3 centers
    N_subj_gr = []  # Number of subjects per group in each center
    for gr in range(len(groups)):
        N_subj_gr.append(int((X.loc[X.label == groups[gr], :].shape[0]) / 3))

    XC_init = []

    index_gr = []
    X_C_gr = X.loc[X.label == groups[0], :].sample(n=N_subj_gr[0], random_state=21)
    index_gr.append(X_C_gr.index)
    for gr in range(1, len(groups)):
        X_C_gr_g = X.loc[X.label == groups[gr], :].sample(n=N_subj_gr[gr], random_state=21)
        index_gr.append(X_C_gr_g.index)
        X_C_gr = pd.concat([X_C_gr, X_C_gr_g], axis=0)
    XC_init.append(X_C_gr)
    for c in range(1, 3 - 1):
        X_C_gr = X.loc[X.label == groups[0], :].drop(index_gr[0], axis=0).sample(n=N_subj_gr[0], random_state=21)
        index_gr[0] = index_gr[0].union(X_C_gr.index)
        for gr in range(1, len(groups)):
            X_C_gr_g = X.loc[X.label == groups[gr], :].drop(index_gr[gr], axis=0).sample(n=N_subj_gr[gr],
                                                                                         random_state=21)
            index_gr[gr] = index_gr[gr].union(X_C_gr_g.index)
            X_C_gr = pd.concat([X_C_gr, X_C_gr_g], axis=0)
        XC_init.append(X_C_gr)
    X_C_gr = X.loc[X.label == groups[0], :].drop(index_gr[0])
    for gr in range(1, len(groups)):
        X_C_gr_g = X.loc[X.label == groups[gr], :].drop(index_gr[gr])
        X_C_gr = pd.concat([X_C_gr, X_C_gr_g], axis=0)
    XC_init.append(X_C_gr)

    # C2 without view 2, C3 without view 3
    # save excluded columns in datasets
    X2_V2 = pd.concat([XC_init[1].iloc[:, 0], XC_init[1].iloc[:, D_i[0] + 1:D_i[0] + D_i[1] + 1]], axis=1)
    XC_init[1] = XC_init[1].drop(XC_init[1].columns[list(range(D_i[0] + 1, D_i[0] + D_i[1] + 1))], axis=1)
    X3_V3 = pd.concat([XC_init[2].iloc[:, 0], XC_init[2].iloc[:, D_i[0] + D_i[1] + 1:D_i[0] + D_i[1] + D_i[2] + 1]],
                      axis=1)
    XC_init[2] = XC_init[2].drop(XC_init[2].columns[list(range(D_i[0] + D_i[1] + 1, D_i[0] + D_i[1] + D_i[2] + 1))],
                                 axis=1)

    # stratify
    skf0 = StratifiedKFold(n_splits=3, random_state=21, shuffle=True)
    for fold_i_0, (train_index_0, test_index_0) in enumerate(
            skf0.split(XC_init[0].drop(columns='label'), XC_init[0].label), 1):
        X_train_0, X_test_0 = XC_init[0].drop(columns='label').iloc[train_index_0, :], XC_init[0].drop(
            columns='label').iloc[test_index_0, :]
        y_train_0, y_test_0 = XC_init[0].label.iloc[train_index_0], XC_init[0].label.iloc[test_index_0]

        skf1 = StratifiedKFold(n_splits=3, random_state=21, shuffle=True)
        for fold_i_1, (train_index_1, test_index_1) in enumerate(
                skf1.split(XC_init[1].drop(columns='label'), XC_init[1].label), 1):
            X_train_1, X_test_1 = XC_init[1].drop(columns='label').iloc[train_index_1, :], XC_init[1].drop(
                columns='label').iloc[test_index_1, :]
            y_train_1, y_test_1 = XC_init[1].label.iloc[train_index_1], XC_init[1].label.iloc[test_index_1]

            skf2 = StratifiedKFold(n_splits=3, random_state=21, shuffle=True)
            for fold_i_2, (train_index_2, test_index_2) in enumerate(
                    skf2.split(XC_init[2].drop(columns='label'), XC_init[2].label), 1):
                X_train_2, X_test_2 = XC_init[2].drop(columns='label').iloc[train_index_2, :], XC_init[2].drop(
                    columns='label').iloc[test_index_2, :]
                y_train_2, y_test_2 = XC_init[2].label.iloc[train_index_2], XC_init[2].label.iloc[test_index_2]

                XC = [X_train_0, X_train_1, X_train_2]
                Y_train = [y_train_0, y_train_1, y_train_2]

                if N_Centers == 6:
                    XC_6 = []
                    Y_train_6 = []
                    for xc in range(len(XC)):
                        X_train_c, X_test_c, y_train_c, y_test_c = train_test_split(XC[xc], Y_train[xc], test_size=0.5,
                                                                                    random_state=21)
                        XC_6.extend([X_train_c, X_test_c])
                        Y_train_6.extend([y_train_c, y_test_c])
                    XC = XC_6
                    Y_train = Y_train_6

                Test_data = [X_test_0, X_test_1, X_test_2]
                Y_test = [y_test_0, y_test_1, y_test_2]

                # save absent views to csv for plots
                X2_V2_test = X2_V2.drop(columns='label').iloc[test_index_1, :]
                X3_V3_test = X3_V3.drop(columns='label').iloc[test_index_2, :]

                X_absent_view = [0, X2_V2_test, X3_V3_test]

                ViewsTest = [[1 for _ in range(len(D_i))] for _ in range(3)]
                ViewsTest[1][1] = 0
                ViewsTest[2][2] = 0
                ViewsXC = [[1 for _ in range(len(D_i))] for _ in range(N_Centers)]
                if N_Centers == 3:
                    ViewsXC[1][1] = 0
                    ViewsXC[2][2] = 0
                elif N_Centers == 6:
                    ViewsXC[2][1] = 0
                    ViewsXC[3][1] = 0
                    ViewsXC[4][2] = 0
                    ViewsXC[5][2] = 0

                data[experiment_i] = {'train_sets': (XC, Y_train, ViewsXC), 'test_set': (Test_data, Y_test, ViewsTest, X_absent_view)}
                experiment_i += 1
    return data
