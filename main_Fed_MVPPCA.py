import sys
import pandas as pd
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
from math import sqrt, ceil
import numpy as np
import os
from os.path import dirname
from sklearn import preprocessing
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import confusion_matrix
from csv import writer
from datetime import datetime
from scipy.stats import norm
import argparse
sys.path.append(dirname(dirname(__file__)))

from utils import t_n_k, Diffs, append_list_as_row, eigen_dec_emp_cov
from Model.Fed_MVPPCA import Fed_MVPPCA_CLIENT, Fed_MVPPCA_MASTER
from Data.Partition_data import split_IID, split_G, split_K, split_GK

matplotlib.use('agg')
sns.set()

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Fed_MVPPCA')
    parser.add_argument('--data', metavar='-d', type=str, default='SD', choices = ['SD', 'ADNI'],
                        help='Dataset')
    parser.add_argument('--scenario', metavar='-s', type=str, default='IID', choices = ['IID', 'G', 'K', 'GK'],
                        help='Scenario for data splitting')
    parser.add_argument('--centers', metavar='-c', type=int, default=4,
                        help='Number of centers')
    parser.add_argument('--rounds', metavar='-r', type=int, default=30,
                        help='Number of rounds')
    parser.add_argument('--epochs', metavar='-e', type=int, default=15,
                        help='Number of epochs')
    parser.add_argument('--latent_dim', metavar='-ld', type=int, default=2,
                        help='Dimension latent space')
    parser.add_argument('--id', metavar='-i', type=str, default='loc',
                        help='Test id')
    parser.add_argument('--norm', metavar='-n', default=True,
                        help='normalize data')
    parser.add_argument('--N_WAIC', metavar='-w', type=int, default=100,
                        help='Number of parameters simulations for WAIC')

    args = parser.parse_args()

    # csv file for records
    script_dir = os.path.dirname(__file__)
    results_csv_name = 'Model_comparison_' + args.scenario + '.csv'
    results_csv = os.path.join(script_dir, results_csv_name)
    if not os.path.isfile(results_csv):
        header = ['Test', 'N_rounds', 'N_epochs', 'D_k', 'N_centers', 'N_subj', 'N_comp',
                  'num_simu', 'WAIC', 'muk_tildemuk', 'Wk_tildeWk',
                  'Sigma2k_tildeSigma2k', 'Eigenval_cov', 'Max_eigen', 'MAE', 'MAE_test',
                  'accuracy_LDA_Test', 'conf_LDA_Test']

        with open(results_csv, "w", newline='') as f:
            writer(f, delimiter=',').writerow(header)  # write the header

    # csv file for saving local and global parameters
    Params_csv_name = 'Estimated_parameters_' + args.scenario + '.csv'
    Params_csv = os.path.join(script_dir, Params_csv_name)
    if not os.path.isfile(Params_csv):
        header = ['Test', 'S_muk', 'S_Wk', 'alphak', 'betak']

        with open(Params_csv, "w", newline='') as f:
            writer(f, delimiter=',').writerow(header)  # write the header

    # initialization

    N_Centers = args.centers # number of centers

    if args.scenario == 'IID':
        data = split_IID(dataset=args.data, N_Centers=N_Centers)
    else:
        assert N_Centers in [3, 6]
        if args.scenario == 'G':
            data = split_G(dataset=args.data, N_Centers=N_Centers)
        elif args.scenario == 'K':
            data = split_K(dataset=args.data, N_Centers=N_Centers)
        elif args.scenario == 'GK':
            data = split_GK(dataset=args.data, N_Centers=N_Centers)

    N_rounds = args.rounds # number of rounds (centers-master)
    CENTERS = range(N_Centers)

    n_comp = args.latent_dim  # number of components
    rho = 1e-2  # this parameter is used exclusively if only one center is contributing to the estimation of one view/group parameters

    groups = data['char']['groups']
    D_i = data['char']['views']
    G = len(groups)  # total number of groups
    K = len(D_i)  # number views

    del data['char']
    print(data.keys())

    for fold in data.keys():
        print(fold)

        # Create datasets for saving results
        results = pd.DataFrame()  # results of local estimations
        Global_res = pd.DataFrame()  # results of global estimations
        Data = pd.DataFrame()  # simulation of x_n|t_n after the last round
        Tksim = pd.DataFrame()  # simulation of t_n(k) using prior distributions after the last round

        # folder for saving figs
        Test_id = datetime.now().strftime('%Y-%m-%d %H:%M:%S') + '_' + args.id  # run id for records

        name_res_dir = 'Fed_MVPPCA_Results_' + args.scenario + '_' + Test_id + '/'
        results_dir = os.path.join(script_dir, name_res_dir)

        if not os.path.isdir(results_dir):
            os.makedirs(results_dir)

        XC = data[fold]['train_sets'][0]
        Y_train = data[fold]['train_sets'][1]
        ViewsXC = data[fold]['train_sets'][2]
        Test_data = data[fold]['test_set'][0]
        Y_test = data[fold]['test_set'][1]
        ViewsTest = data[fold]['test_set'][2]

        if args.scenario in ('K','GK'):
            X_absent_view = data[fold]['test_set'][3]
            # save absent views to csv for plots
            X_absent_view[1].to_csv(results_dir + "C2_V2.csv")
            X_absent_view[2].to_csv(results_dir + "C3_V3.csv")

        CENTERS = range(N_Centers)
        N_subjects = [xc.shape[0] for xc in XC]
        N_epochs = [args.epochs for c in CENTERS]

        # Save first columns of t_n_k to csv for plots
        Tk = t_n_k(XC, ViewsXC, D_i, CENTERS, K, args.norm)
        Tk.to_csv(results_dir + "Tnk.csv")

        Eigenval_cov = []
        Max_eigen = []

        ROUNDS = range(1, N_rounds + 1)
        G_params = [[0] for _ in range(7)]  # for the first round no prior is given

        # ====================================== #
        #  ITERATION CENTERS-MASTER STARTS HERE  #
        # ====================================== #
        for r in ROUNDS:
            print(f'Round {r}')
            L_params_new = [[], [], []]  # order = muk, Wk, A, Sigma2
            for c in CENTERS:
                local_model = Fed_MVPPCA_CLIENT(XC[c], ViewsX=ViewsXC[c], norm=args.norm, global_params=G_params,
                                                n_views=K, dim_views=D_i, n_components=n_comp,
                                                n_iterations=N_epochs[c], id_client=c, round=r)
                results = results.append(local_model.fit(), ignore_index=True)
                muk, Wk, Sigma2 = local_model.local_params
                L_params_new[0].append(muk)
                L_params_new[1].append(Wk)
                L_params_new[2].append(Sigma2)
                if r == N_rounds:
                    # simulation of x_n|t_n using local parameters
                    X_simu_g = local_model.simuXn().reshape(n_comp, 1)
                    for s in range(499):
                        Xsimg = local_model.simuXn().reshape(n_comp, 1)
                        X_simu_g = np.concatenate((X_simu_g, Xsimg), axis=1)
                    len_X_simu_g = len(X_simu_g[0, :])
                    Xn_giv_tn = pd.DataFrame({'X_n_0': list(X_simu_g[0, :]), 'X_n_1': list(X_simu_g[1, :]),
                                              'Center': [(c + 1) for _ in range(len_X_simu_g)]})
                    Data = Data.append(Xn_giv_tn, ignore_index=True)
            results['center'] = pd.Categorical(results['center'], ordered=True,
                                               categories=[x + 1 for x in CENTERS])
            print(results['center'].value_counts())
            global_model = Fed_MVPPCA_MASTER(local_params=L_params_new, n_centers=N_Centers, ViewsC=ViewsXC,
                                             n_views=K, dim_views=D_i, n_components=n_comp,
                                             rho=rho, round=r)
            Global_res = Global_res.append(global_model.fit(), ignore_index=True)
            tilde_muk, tilde_Wk, tilde_Sigma2k, Alpha, Beta, sigma_til_muk, sigma_til_Wk = global_model.global_params
            G_params = [tilde_muk, tilde_Wk, tilde_Sigma2k, Alpha, Beta, sigma_til_muk, sigma_til_Wk]
        # ====================================== #
        # ====================================== #

        # simulation of t_n(k)
        X_simu = []
        for k in range(K):
            X_simu_k = []
            for s in range(1000):
                X_simu_k.append(global_model.simuTnk(k))
            X_simu.append(X_simu_k)
            # Estimation empirical covariance
            Eigenval_cov.append(eigen_dec_emp_cov(X_simu_k, D_i, k))
            Max_eigen.append(max(Eigenval_cov[k]))
        for k in range(K):
            Xs = X_simu[k][0]
            for s in range(1, len(X_simu[k])):
                Xs = np.concatenate((Xs, X_simu[k][s]), axis=1)
            Tn_k = pd.DataFrame({'T_n_0': list(Xs[0, :]), 'T_n_1': list(Xs[1, :]),
                                 'View': [(k + 1) for _ in range(len(Xs[0, :]))]})
            Tksim = Tksim.append(Tn_k, ignore_index=True)

        # save simulations of t_n(k) to csv
        Tksim.to_csv(results_dir + "Tnksim.csv")

        # save simulations of x_n|t_n and x_n|t_n(k) to csv
        Data.to_csv(results_dir + "Xn_given_tn.csv")

        # save local results to csv
        results.to_csv(results_dir + "Centers_results.csv")

        # save global results to csv
        Global_res.to_csv(results_dir + "Master_results.csv")

        # append estimated parameters to csv
        # List of strings
        row_contents = [Test_id, G_params[5], G_params[6], G_params[3], G_params[4]]
        # Append row_contents as new line to Model_comparaison.csv
        append_list_as_row(Params_csv, row_contents)

        tot_simu = []
        WAIC_score = []
        MAE = []
        Latent_Train = pd.DataFrame()
        Label_Train = pd.Series()
        for c in CENTERS:
            local_model = Fed_MVPPCA_CLIENT(XC[c], ViewsX=ViewsXC[c], norm=args.norm, global_params=G_params,
                                            n_views=K, dim_views=D_i, n_components=n_comp, n_iterations=0,
                                            id_client=c, round=N_rounds + 1)
            # Dataframe of latent space for LDA
            Latent_Train = Latent_Train.append(local_model.simu_latent())
            Label_Train = Label_Train.append(Y_train[c])
            # Mean Absolute Error
            MAE.append(local_model.MAE())
            # WAIC
            WAIC, newS = local_model.WAIC_score(args.N_WAIC)
            WAIC_score.append(WAIC)
            tot_simu.append(newS)

        lda = LinearDiscriminantAnalysis()
        X_Train_lda = lda.fit_transform(Latent_Train, Label_Train.reindex(Latent_Train.index))

        # Evaluate global accuracy for test dataset
        MAE_test = []
        Latent_Test = pd.DataFrame()
        Label_Test = pd.Series()
        for c_test in range(len(Test_data)):
            local_model = Fed_MVPPCA_CLIENT(Test_data[c_test], ViewsX=ViewsTest[c_test], norm=args.norm,
                                            global_params=G_params, n_views=K, dim_views=D_i,
                                            n_components=n_comp, n_iterations=0, id_client=99 + c_test, round=1)
            # Dataframe of latent space for LDA
            Latent_Test = Latent_Test.append(local_model.simu_latent())
            Label_Test = Label_Test.append(Y_test[c_test])
            # MAE Test
            MAE_test.append(local_model.MAE())
        # LDA Test
        Size_tes = Latent_Test.shape[0]
        y_pred_test = lda.predict(Latent_Test)
        conf_LDA_Test = confusion_matrix(Label_Test.reindex(Latent_Test.index), y_pred_test)
        TP = np.diag(conf_LDA_Test)
        num_classes = len(np.unique(Label_Test))
        accuracy_LDA_Test = sum(TP) / Size_tes

        # Difference among estimated local and global parameters
        Diff_1,Diff_2,Diff_3 = Diffs(G_params, L_params_new, CENTERS, ViewsXC, K)

        # Append results to csv for model comparaison
        # List of strings
        row_contents = [Test_id, N_rounds, N_epochs, D_i, N_Centers, N_subjects, n_comp,
                        tot_simu, WAIC_score, Diff_1, Diff_2, Diff_3, Eigenval_cov, Max_eigen, MAE, MAE_test,
                        accuracy_LDA_Test, conf_LDA_Test]
        # Append row_contents as new line to Model_comparaison.csv
        append_list_as_row(results_csv, row_contents)


        # Print and save true vs prediction for excluded views in test dataset, and save for plotting
        if args.scenario in ('K', 'GK'):
            Pred_csv_name = 'Prediction_k_' + args.scenario + '_' + '.csv'
            pred_k_csv = os.path.join(script_dir, Pred_csv_name)
            if not os.path.isfile(pred_k_csv):
                header = ['Test_id', 'Center_id', 'View_id', 'meank', 'Diag_V']
                with open(pred_k_csv, "w", newline='') as f:
                    writer(f, delimiter=',').writerow(header)  # write the header

            tilde_muk = G_params[0]
            tilde_Wk = G_params[1]
            tilde_Sigma2k = G_params[2]
            for c in range(len(Test_data)):
                if sum(ViewsTest[c]) < K:
                    if args.norm == True:
                        x = X_absent_view[c].values
                        min_max_scaler = preprocessing.MinMaxScaler()
                        x_scaled = min_max_scaler.fit_transform(x)
                        X_absent_view[c] = pd.DataFrame(x_scaled, index=X_absent_view[c].index,
                                                        columns=X_absent_view[c].columns)
                    N_sbj = X_absent_view[c].shape[0]
                    for k in range(K):
                        if ViewsXC[c][k] == 0:
                            plt.figure(figsize=(5 * 4, 5 * ceil(D_i[k] / 4)), dpi=150)
                            meank = tilde_muk[k]
                            V = tilde_Wk[k].dot(tilde_Wk[k].T) + tilde_Sigma2k[k] * np.eye(D_i[k])
                            Diag_V = V.diagonal()
                            row_contents = [Test_id, c, k, meank, Diag_V]
                            append_list_as_row(pred_k_csv, row_contents)
                            ind = 1
                            for d in range(D_i[k]):
                                plt.subplot(ceil(D_i[k] / 4), 4, ind).set_title('$(t^{(%i)})_{%i}$' % ((k + 1), d))
                                mean = meank[d][0]
                                std = sqrt(Diag_V[d])
                                plt.hist(X_absent_view[c].iloc[:, d].tolist(), density=True, alpha=0.6,
                                         color='grey', label='data')
                                xmin = min(X_absent_view[c].iloc[:, d].tolist()) - 0.2
                                xmax = max(X_absent_view[c].iloc[:, d].tolist()) + 0.2
                                x = np.linspace(xmin, xmax, 100)
                                y = norm.pdf(x, mean, std)
                                plt.plot(x, y, linewidth=3, color='blue', label='prediction, view = %i' % (k + 1))
                                ind += 1
                            plt.legend()
                            plt.savefig(results_dir + "Missing_view_pred_k=%i_c=%i" % ((k + 1), (c + 1)))
                            plt.clf()
                            plt.close()
