import numpy as np
from numpy.linalg import solve, LinAlgError
import pandas as pd
from math import log
import random
from sklearn import preprocessing
from sklearn.metrics import mean_absolute_error
from scipy.linalg import block_diag
from scipy.special import digamma, polygamma
from scipy.stats import matrix_normal, multivariate_normal, invgamma
from scipy.special import logsumexp

# CENTER

class Fed_MVPPCA_CLIENT:
    def __init__(self, X, ViewsX=[], norm = True, global_params = [], n_views=2, dim_views=[1,1], n_components=2, n_iterations=10, id_client = 1, round = 1):
        self.dataset = X # data
        self.dataset.columns = [col.strip() for col in list(self.dataset.columns)]
        self.n_views = n_views # total number of views per subj in all centers
        self.dim_views = dim_views # dimension of each view
        self.ViewsX = ViewsX  # views measured for current center: ViewsX[k]=1 if the k-th view has been measured, =0 otherwise
        self.index = self.ViewsX.index(1)  # first view measured in current center
        self.n_components = n_components # number of components latent space
        self.n_iterations = n_iterations # number of iterations
        self.id_client = id_client # id center
        self.round = round  # round in Fed algorithm

        self.norm = norm  # If the dataset should be normalized or not
        if self.norm == True:
            x = self.dataset.values  # returns a numpy array
            min_max_scaler = preprocessing.MinMaxScaler()
            x_scaled = min_max_scaler.fit_transform(x)
            self.dataset = pd.DataFrame(x_scaled, index=self.dataset.index, columns=self.dataset.columns)

        self.q_i = [] # in this list we store the "effective" latent space dimension per view
        for i in self.dim_views:
            if i <= self.n_components:
                self.q_i.append(i - 1)
            else:
                self.q_i.append(self.n_components)

        self.Xk = []
        print(f'Center {self.id_client + 1}')
        ind = 0
        for k in range(self.n_views):
            if self.ViewsX[k] == 1:
                self.Xk.append(self.dataset.iloc[:, ind:ind + self.dim_views[k]])
                if self.round == 1:
                    print(self.Xk[k].head())
                    print(f'Shape of X%i: {self.Xk[k].shape}' % k)
                ind += self.dim_views[k]
            else:
                self.Xk.append(0)

        if self.round == 1:
            print(f'Indicator function views = {self.ViewsX}')

        self.mean_tnk = []
        for k in range(self.n_views):
            if self.ViewsX[k] == 1:
                self.mean_tnk.append(self.Xk[k].mean(axis=0).values.reshape(self.dim_views[k], 1))
            else:
                self.mean_tnk.append(0)

        # global parameters
        self.global_params = global_params  # order: tilde_muk, tilde_Wk, tilde_Sigma2k, Alpha, Beta, tilde_Ag, sigma_til_muk, sigma_til_Wk, sigma_til_Ag
        self.tilde_muk = self.global_params[0]
        self.tilde_Wk = self.global_params[1]
        self.tilde_Sigma2k = self.global_params[2]
        self.Alpha = self.global_params[3]
        self.Beta = self.global_params[4]
        self.sigma_til_muk = self.global_params[5]
        self.sigma_til_Wk = self.global_params[6]

        # parameters to be optimized
        self.muk = None
        self.Wk = None
        self.Sigma2 = None

    def fit(self):
        d = self.dataset.shape[1]
        q = self.n_components
        D_i = self.dim_views
        K = self.n_views

        assert K == len(D_i) # Number of classes should be equal to number of classes lenghts

        D_i_C = 0
        for k in range(K):
            if self.ViewsX[k] == 1:
                D_i_C += D_i[k]

        assert d == D_i_C # Dimension of t_n should be equal to sum of views dimensions for current center

        if q > min(D_i):
            print("Warning: number of components greater than view dimensions")
            print(self.q_i)

        Wk, Sigma2 = self.initial_loc_params()

        # Create list of historic results as dataframe
        results_df = pd.DataFrame()

        # epochs indices for saving results
        sp_arr = np.arange(1, self.n_iterations + 1)[np.round(np.linspace(0, len(np.arange(1, self.n_iterations + 1)) - 1, int(self.n_iterations/3))).astype(int)]

        # ================================== #
        #  ITERATION STARTS HERE             #
        # ================================== #
        for i in range(1, self.n_iterations + 1):

            # mu
            muk = self.eval_muk(Wk,Sigma2)
            # matreces M, B
            M, B = self.eval_MB(Wk, Sigma2)
            # ||tn^kg-mu^k)||2, (tn^kg-mu^k)
            norm2, tn_muk = self.compute_access_vectors(muk)

            # ================================== #
            # E-step                             #
            # ================================== #

            # evaluate mean, second moment for each x_n and expected log-likelihood
            E_X, E_X_2, E_L_c = self.compute_moments_LL(Sigma2, norm2, tn_muk, Wk, M, B)

            # ================================== #
            # M-step                             #
            # ================================== #

            #  W, Sigma2
            Wk, Sigma2_new = self.eval_Wk_Sigma2_new(norm2, tn_muk, E_X, E_X_2, Sigma2)
            # Check Sigma2_new>0
            for k in range(K):
                if Sigma2_new[k] > 0:
                    Sigma2[k] = Sigma2_new[k]
                else:
                    print(f'Warning: sigma2(%i)<0 (={Sigma2_new[k]})' % (k + 1))

            # ================================== #
            # Append results                     #
            # ================================== #

            if i in sp_arr:
                results_iter = self.append_results_local(i, E_L_c, Wk, muk, Sigma2)
                results_df = results_df.append(results_iter)

        # update local parameters
        self.muk = muk
        self.Wk = Wk
        self.Sigma2 = Sigma2

        return results_df

    @property
    def local_params(self):
        # Return local parameters
        return self.muk, self.Wk, self.Sigma2

    def Sample_LP(self,k):
        # sample local parameters from global distribution
        q = self.n_components
        D_i = self.dim_views

        Wk = matrix_normal.rvs(mean=self.tilde_Wk[k],
                                rowcov=np.eye(D_i[k]),
                                colcov=self.sigma_til_Wk[k] * np.eye(q)).reshape(D_i[k], q)
        muk = multivariate_normal.rvs(mean=self.tilde_muk[k].reshape(D_i[k]),
                                       cov=self.sigma_til_muk[k] * np.eye(D_i[k])).reshape(
            D_i[k], 1)
        sigmak = float(invgamma.rvs(a=self.Alpha[k], scale=self.Beta[k]))

        return Wk, muk, sigmak

    def Sample_C_musk(self):
        # sample local parameters from global distribution and return matrix C (d x d) and vector mu (d x 1)
        K = self.n_views
        D_i = self.dim_views

        Wsk, musk, sigmaks = self.Sample_LP(self.index)
        Psi = sigmaks * np.eye(D_i[self.index])
        for k in range(self.index + 1, K):
            if self.ViewsX[k] == 1:
                Wsk_k, musk_k, sigmaks = self.Sample_LP(k)
                Wsk = np.concatenate((Wsk, Wsk_k), axis=0)
                musk = np.concatenate((musk, musk_k), axis=0)
                Psi = block_diag(Psi, sigmaks * np.eye(D_i[k]))
        C = Wsk.dot(Wsk.T) + Psi

        return C, musk

    def WAIC_score(self, S):
        # Return WAIC for each center
        d = self.dataset.shape[1]
        N = self.dataset.shape[0]

        elppd2 = 0.0

        logP_N = [[] for _ in range(N)]
        for s in range(S):
            Cs, mus = self.Sample_C_musk()
            for n in range(N):
                try:
                    logP_N[n].append(
                        multivariate_normal.logpdf(self.dataset.iloc[n].values.reshape(d), mus.reshape(d), Cs))
                except LinAlgError:
                    pass

        len_logP_N = [len(logP_N[n]) for n in range(N)]

        for n in range(N):
            if len(logP_N[n])>0:
                LSE = logsumexp(logP_N[n])
                VarP = np.var(logP_N[n])
                elppd2 += LSE - VarP - log(len_logP_N[n])

        if min(len_logP_N) < S:
            print(f'Number of simulations for WAIC: {min(len_logP_N)}')

        WAIC = -2*elppd2

        return WAIC, min(len_logP_N)

    def Glob_Params_complete_data(self):
        # Return tilde_W (d x q), tilde_mu (d x 1), tilde_Psi (d x d)
        K = self.n_views
        D_i = self.dim_views

        TildeW = self.tilde_Wk[self.index]
        Tildemu = self.tilde_muk[self.index]
        TildePsi = self.tilde_Sigma2k[self.index] * np.eye(D_i[self.index])
        for k in range(self.index + 1, K):
            if self.ViewsX[k] == 1:
                TildeW = np.concatenate((TildeW, self.tilde_Wk[k]), axis=0)
                Tildemu = np.concatenate((Tildemu, self.tilde_muk[k]), axis=0)
                TildePsi = block_diag(TildePsi, self.tilde_Sigma2k[k] * np.eye(D_i[k]))

        return TildeW, Tildemu, TildePsi

    def concat_params(self, park):
        # Concatenate parameters from a list
        K = self.n_views

        par = park[self.index]
        for k in range(self.index + 1, K):
            if self.ViewsX[k] == 1:
                par = np.concatenate((par, park[k]), axis=0)

        return par

    def simuXn(self):
        # Generate x_n using x_n|t_n
        d = self.dataset.shape[1]
        N = self.dataset.shape[0]

        mu = self.concat_params(self.muk)
        M, B = self.eval_MB(self.Wk, self.Sigma2)

        ran_n = random.randint(0, N - 1)

        Xng = np.random.multivariate_normal(mean=list((M.dot(B).dot(self.dataset.iloc[ran_n].values.reshape(d, 1) - mu)).T[0]), cov=M)

        return Xng

    def simu_latent(self):
        # Sample latent space from posterior
        d = self.dataset.shape[1]
        q = self.n_components
        N = self.dataset.shape[0]

        mu = self.concat_params(self.tilde_muk)
        M, B = self.eval_MB(self.tilde_Wk, self.tilde_Sigma2k)

        Xn = [(M.dot(B).dot(self.dataset.iloc[n].values.reshape(d, 1) - mu)).reshape(1, q) for n in range(N)]

        df = pd.DataFrame(np.vstack(Xn), index=self.dataset.index)

        return df

    def simuXnk(self, view):
        # Generate x_n using x_n|t_n(k)
        q = self.n_components
        D_i = self.dim_views
        N = self.dataset.shape[0]

        M = solve(self.Sigma2[view] * np.eye(q) + self.Wk[view].reshape(D_i[view], q).T.dot(
            self.Wk[view].reshape(D_i[view], q)),np.eye(q))

        ran_n = random.randint(0, N - 1)

        Xngk = np.random.multivariate_normal(mean=list((M.dot(self.Wk[view].reshape(D_i[view], q).T).dot(
            self.Xk.iloc[ran_n].values.reshape(D_i[view], 1) - self.muk[view])).T[0]),
                                             cov=self.Sigma2[view] * M)

        return Xngk

    def MAE(self):
        # Evaluate MAE
        d = self.dataset.shape[1]
        q = self.n_components
        N = self.dataset.shape[0]

        mu = self.concat_params(self.tilde_muk)
        W = self.concat_params(self.tilde_Wk)
        M, B = self.eval_MB(self.tilde_Wk, self.tilde_Sigma2k)

        T_true = self.dataset.values.tolist()

        T_pred = []
        for n in range(N):
            Xng = (M.dot(B).dot(self.dataset.iloc[n].values.reshape(d, 1) - mu)).reshape(q, 1)
            T_pred.append((W.dot(Xng) + mu).reshape(d))

        MAE = mean_absolute_error(T_true, T_pred)

        return MAE

    def pred_missing_view(self,view):
        # Prediction of missing view
        d = self.dataset.shape[1]
        D_i = self.dim_views
        q = self.n_components
        N = self.dataset.shape[0]

        mu = self.concat_params(self.tilde_muk)
        M, B = self.eval_MB(self.tilde_Wk, self.tilde_Sigma2k)

        T_pred = []
        for n in range(N):
            Xng = (M.dot(B).dot(self.dataset.iloc[n].values.reshape(d, 1) - mu)).reshape(q, 1)
            T_pred.append((self.tilde_Wk[view].dot(Xng) + self.tilde_muk[view]).reshape(D_i[view]))

        df = pd.DataFrame(np.row_stack(T_pred))

        return df


    def initial_loc_params(self):
        # Initialize local parameters
        q = self.n_components
        D_i = self.dim_views
        K = self.n_views

        if self.round == 1:
            # initialize W, Sigma2 randomly
            Wk = []
            Sigma2 = []
            ind = 1
            for k in range(K):
                if self.ViewsX[k] == 1:
                    W_k = np.random.uniform(self.Xk[k].min().min(),self.Xk[k].max().max(), size = D_i[k]*q).reshape([D_i[k],q])
                    if self.q_i[k]<q:
                        W_k[:, self.q_i[k]:q] = 0
                    Wk.append(W_k)
                    Sigma2.append(random.uniform(0.1,0.5))
                    ind += D_i[k]
                else:
                    Wk.append(0)
                    Sigma2.append(1)
        else:
            # initialize W, Sigma2 using updated prior
            Wk = []
            Sigma2 = []
            for k in range(K):
                if self.ViewsX[k] == 1:
                    W_k = matrix_normal.rvs(mean=self.tilde_Wk[k].reshape(D_i[k], q),
                                                rowcov=np.eye(D_i[k]),
                                                colcov=self.sigma_til_Wk[k]*np.eye(q)).reshape(D_i[k], q)
                    if self.q_i[k] < q:
                        W_k[:, self.q_i[k]:q] = 0
                    Wk.append(W_k)
                    Sigma2.append(float(invgamma.rvs(a=self.Alpha[k], scale=self.Beta[k])))
                else:
                    Wk.append(0)
                    Sigma2.append(1)

        return Wk, Sigma2

    def eval_muk(self,Wk,Sigma2):
        """
        Computes local parameter muk.
        :param Wk: list of matrices (d_k x q)
        :param Sigma2: list of float > 0
        """

        N = self.dataset.shape[0]
        D_i = self.dim_views
        K = self.n_views

        muk = []

        for k in range(K):
            if self.ViewsX[k] == 1:
                if self.round == 1:
                    muk.append(self.mean_tnk[k].reshape(D_i[k], 1))
                else:
                    mu_1 = np.zeros((D_i[k], 1))
                    for n in range(N):
                        mu_1 += self.Xk[k].iloc[n].values.reshape(D_i[k], 1)
                    term1 = self.compute_inv_term1_muck(Wk[k], N, Sigma2[k], self.sigma_til_muk[k])
                    Cc = self.compute_Cck(Wk[k], Sigma2[k])
                    term2 = mu_1 + (1 / self.sigma_til_muk[k]) * Cc.dot(self.tilde_muk[k].reshape(D_i[k], 1))
                    muk.append(term1.dot(term2))
            else:
                muk.append(0)

        return muk

    def eval_MB(self, Wk, Sigma2):
        """
        Computes matrices M:=inv(I_q+sum_k Wk.TWk/sigma2k) and B:= [W1.T/sigma2K,...,W1.T/sigma2K].
        :param Wk: list of matrices (d_k x q)
        :param Sigma2: list of float > 0
        """

        q = self.n_components
        D_i = self.dim_views
        K = self.n_views

        M1 = Wk[self.index].reshape(D_i[self.index], q).T.dot(Wk[self.index].reshape(D_i[self.index],q)) / Sigma2[self.index]
        B = Wk[self.index].reshape(D_i[self.index], q).T / Sigma2[self.index]
        for k in range(self.index + 1, K):
            if self.ViewsX[k] == 1:
                M1 += Wk[k].reshape(D_i[k], q).T.dot(Wk[k].reshape(D_i[k],q)) / Sigma2[k]
                B = np.concatenate((B, (Wk[k].reshape(D_i[k], q)).T / Sigma2[k]), axis=1)

        M = solve(np.eye(q) + M1,np.eye(q))

        return M, B

    def compute_access_vectors(self,muk):

        """
        Computes list of accessory vectors needed for further evaluations: norm**2 of (tn^kg-mu^k-W^kag), (tn^kg-mu^k-W^kag), (tn^kg-mu^k).
        :param muk: list of vectors (d_k x 1)
        :param Wk: list of matrices (d_k x q)
        """

        N = self.dataset.shape[0]
        D_i = self.dim_views
        K = self.n_views

        norm2 = [] # norm**2 of (tn^kg-mu^k)
        tn_muk = [] # (tn^kg-mu^k)
        for n in range(N):
            norm2_k = []
            tn_mu_k = []
            for k in range(K):
                if self.ViewsX[k] == 1:
                    tn_mu_k.append(self.Xk[k].iloc[n].values.reshape(D_i[k], 1) - muk[k])
                    norm2_k.append(np.linalg.norm(tn_mu_k[k]) ** 2)
                else:
                    norm2_k.append(0)
                    tn_mu_k.append(0)
            norm2.append(norm2_k)
            tn_muk.append(tn_mu_k)

        return norm2, tn_muk

    def compute_moments_LL(self, Sigma2, norm2, tn_muk, Wk, M, B):
        """
        Computes mean and second moment for each x_n, then expected log-likelihood.
        :param Sigma2: list of floats > 0
        :param norm2: list of floats: norm**2 of (tn^kg-mu^k)
        :param tn_muk: list of vectors (d_k x 1)
        :param Wk: list of matrices (d_k x q)
        :param M: matrix (q x q)
        :param B: matrix (q x d_k)
        """

        q = self.n_components
        N = self.dataset.shape[0]
        D_i = self.dim_views
        K = self.n_views

        E_X = []
        E_X_2 = []
        E_L_c = 0.0

        for n in range(N):
            tn_mu = tn_muk[n][self.index]
            for k in range(self.index+1,K):
                if self.ViewsX[k] == 1:
                    tn_mu = np.concatenate((tn_mu, tn_muk[n][k]), axis=0)
            E_X.append(M.dot(B).dot(tn_mu))

            E_X_2.append(M + E_X[n].dot(E_X[n].T))

            E_L_c_k = 0.0
            for k in range(K):
                if self.ViewsX[k] == 1:
                    E_L_c_k += - D_i[k] * log(Sigma2[k])/ 2.0 - (norm2[n][k] / 2 + np.matrix.trace(
                        (Wk[k].reshape(D_i[k], q)).T.dot(Wk[k].reshape(D_i[k], q)) * E_X_2[n]) / 2 - E_X[n].T.dot(
                        (Wk[k].reshape(D_i[k], q)).T).dot(tn_muk[n][k])) / Sigma2[k]
            E_L_c += float(E_L_c_k  - np.matrix.trace(E_X_2[n]) / 2.0)

        return E_X, E_X_2, E_L_c

    def eval_Wk_Sigma2_new(self, norm2, tn_muk, E_X, E_X_2, Sigma2):
        """
        Computes local parameter Wk and Sigma2k.
        :param norm2: list of floats: norm**2 of (tn^kg-mu^k)
        :param tn_muk: list of vectors (d_k x 1): (tn^kg-mu^k)
        :param E_X: list of vectors (q x 1): mean of x_n
        :param E_X_2: list of matrices (q x q): second moment of x_n
        :param Sigma2: list of floats > 0
        """

        q = self.n_components
        N = self.dataset.shape[0]
        D_i = self.dim_views
        K = self.n_views

        Wk = []
        Sigma2_new = []

        for k in range(K):
            if self.ViewsX[k] == 1:
                W_1_1 = (tn_muk[0][k]).dot(E_X[0].T)
                W_2_2 = sum(E_X_2)
                for n in range(1,N):
                    W_1_1 += (tn_muk[n][k]).dot(E_X[n].T)

                if self.round == 1:
                    W_1 = W_1_1

                    W_2 = solve(W_2_2, np.eye(q))
                else:
                    W_1 = W_1_1 + (Sigma2[k] / self.sigma_til_Wk[k]) * self.tilde_Wk[k]

                    W_2 = solve(W_2_2 + (Sigma2[k] / self.sigma_til_Wk[k]) * np.eye(q), np.eye(q))

                W_k = W_1.dot(W_2)
                if self.q_i[k] < q:
                    W_k[:, self.q_i[k]:q] = 0
                Wk.append(W_k)

                sigma2k = 0.0
                for n in range(N):
                    sigma2k += float(norm2[n][k] + np.matrix.trace(
                        (Wk[k].reshape(D_i[k], q)).T.dot(Wk[k].reshape(D_i[k], q)) * E_X_2[n]) - 2 * E_X[n].T.dot(
                        (Wk[k].reshape(D_i[k], q)).T).dot(tn_muk[n][k]))
                if self.round == 1:
                    var = 1  # variance of the Inverse-Gamma prior
                    alpha = 1.0 / (4 * var) + 2
                    beta = (alpha - 1) / 2
                    sigma2k_N = (sigma2k + 2 * beta) / (N * D_i[k] + 2 * (alpha + 1))  ## prior=inverse gamma
                    while sigma2k_N <= 0:  # while til obtention of a non negative sigma2k: each round the variance of the Inverse Gamma is divided by 2
                        var *= 1.0 / 2
                        alpha = 1.0 / (4 * var) + 2
                        beta = (alpha - 1) / 2
                        sigma2k_N = (sigma2k + 2 * beta) / (N * D_i[k] + 2 * (alpha + 1))  ## prior=inverse gamma
                    if var != 1:
                        print(f'Variance of Inverse-Gamma for sigma2(%i) = {var}' % (k + 1))
                    Sigma2_new.append(sigma2k_N)
                else:
                    Sigma2_new.append((sigma2k + 2 * self.Beta[k]) / (N * D_i[k] + 2 * (self.Alpha[k] + 1)))
            else:
                Wk.append(0)
                Sigma2_new.append(1)

        return Wk, Sigma2_new

    def append_results_local(self, i, E_L_c, Wk, muk, Sigma2):
        """
        Append all results for record.
        :param i: int (iteration id)
        :param E_L_c: float (expected log-likelihood)
        :param Wk: list of matrices (d_k x q)
        :param muk: list of vectors (d_k x 1)
        :param Sigma2: list of floats > 0
        """

        K = self.n_views

        results_iter = pd.Series(name=i, dtype=float)
        results_iter['iter'] = i+self.n_iterations*(self.round-1)
        results_iter['E_L_c'] = E_L_c
        for k in range(K):
            name1 = 'W_norm_' + str(k + 1)
            name2 = 'mu_norm_' + str(k + 1)
            name3 = 'sigma2_' + str(k + 1)
            if self.ViewsX[k] == 1:
                results_iter[name1] = np.linalg.norm(Wk[k])
                results_iter[name2] = np.linalg.norm(muk[k])
                results_iter[name3] = Sigma2[k]
            else:
                results_iter[name1] = 0
                results_iter[name2] = 0
                results_iter[name3] = 0
        results_iter['center'] = self.id_client+1

        return results_iter

    @staticmethod
    def compute_Cck(Wk, Sigk):
        """
        Computes matrix Ck.
        :param Wk: matrix (d_k x q)
        :param Sigk: float > 0
        :return np.array : matrix Cck
        """

        dk = Wk.shape[0]

        Cck = Wk.dot(Wk.T)+Sigk*np.eye(dk)

        return Cck

    @staticmethod
    def compute_inv_term1_muck(Wk, Nc, Sigk, Til_Sigk):
        """
        :param Wk: matrix (d_k x q)
        :param Nc: float > 0
        :param Sigk: float > 0
        :param Til_Sigk: float > 0
        :return np.array : inverse of first term to evaluate muck, using the Woodbury matrix identity
        """

        dk = Wk.shape[0]
        q = Wk.shape[1]

        k = 1.0/(Nc*Til_Sigk+Sigk)

        Inverse = solve(np.eye(q)+k*Wk.T.dot(Wk), np.eye(q))

        inverse_term1 = k*Til_Sigk*(np.eye(dk)-k*Wk.dot(Inverse).dot(Wk.T))

        return inverse_term1

# MASTER

class Fed_MVPPCA_MASTER:
    def __init__(self, local_params=[], n_centers=2, ViewsC = [], n_views=2, dim_views=[], n_components=2, rho=0.05, round = 1):
        self.n_views = n_views # number of views per subj
        self.dim_views = dim_views # dimension of each view
        self.n_components = n_components # number of components latent space
        self.n_centers = n_centers # total number of centers
        self.ViewsC = ViewsC # views measured in each center

        self.rho = rho

        self.round = round

        self.local_params = local_params
        self.mukC = self.local_params[0]
        self.WkC = self.local_params[1]
        self.Sigma2kC = self.local_params[2]

        # Global parameters, to be updated
        self.tilde_muk = None
        self.tilde_Wk = None
        self.tilde_Sigma2k = None
        self.Alpha = None
        self.Beta = None
        self.sigma_til_muk = None
        self.sigma_til_Wk = None

    def fit(self):
        q = self.n_components
        D_i = self.dim_views
        K = self.n_views
        C = self.n_centers

        corr_det_inv = 1e-35

        # Create list of historic results as dataframe
        results_df = pd.DataFrame()

        # total number of centers having measurements for each view
        Tot_C_k = self.count_views()

        # tilde_muk
        tilde_muk = []
        # tilde_Wk
        tilde_Wk = []
        # tilde_Sigma2k
        tilde_Sigma2k = []
        # variance muk
        sigma_til_muk = []
        # variance Wk
        sigma_til_Wk = []
        for k in range(K):
            tilmuk = np.zeros((D_i[k], 1))
            tilWk = np.zeros((D_i[k], q))
            tilSk = 0.0
            for c in range(C):
                if self.ViewsC[c][k] == 1:
                    tilmuk+=self.mukC[c][k]
                    tilWk += self.WkC[c][k]
                    tilSk += self.Sigma2kC[c][k]
            tilde_muk.append(1.0/Tot_C_k[k]*tilmuk)
            tilde_Wk.append(1.0 / Tot_C_k[k] * tilWk)
            tilde_Sigma2k.append(tilSk / Tot_C_k[k])
            if Tot_C_k[k] > 1:
                sigmuk = 0.0
                sigWk = 0.0
                for c in range(C):
                    if self.ViewsC[c][k] == 1:
                        sigmuk+=float((self.mukC[c][k]-tilde_muk[k]).T.dot(self.mukC[c][k]-tilde_muk[k]))
                        sigWk += np.matrix.trace((self.WkC[c][k] - tilde_Wk[k]).T.dot(self.WkC[c][k] - tilde_Wk[k]))
                if sigmuk == 0.0:
                    sigma_til_muk.append(corr_det_inv)
                else:
                    sigma_til_muk.append(sigmuk/(Tot_C_k[k]*D_i[k]))
                if sigWk == 0.0:
                    sigma_til_Wk.append(corr_det_inv)
                else:
                    sigma_til_Wk.append(sigWk / (Tot_C_k[k] * D_i[k] * q))
            else:
                sigma_til_Wk.append(self.rho)
                sigma_til_muk.append(self.rho)

        # Alpha, Beta ML method
        Alpha = []
        Beta = []
        for k in range(K):
            Ck_1 = 0.0
            Ck_2 = 0.0
            varSk = 0.0
            for c in range(C):
                if self.ViewsC[c][k] == 1:
                    Ck_1 += 1.0 / self.Sigma2kC[c][k]
                    Ck_2 += log(self.Sigma2kC[c][k])
                    varSk += (self.Sigma2kC[c][k] - tilde_Sigma2k[k]) ** 2
            Ck = -log(Ck_1) - Ck_2 / Tot_C_k[k]
            if varSk == 0.0:
                varSk = 1e-16
            if Tot_C_k[k] == 1:
                alphak = (tilde_Sigma2k[k] ** 2) / (varSk) + 2
            else:
                alphak = (tilde_Sigma2k[k] ** 2) / (varSk / (Tot_C_k[k] - 1.0)) + 2
            for cov in range(10):
                alphak = self.inv_digamma(y=log(Tot_C_k[k]*alphak)+Ck)
            betak = (Tot_C_k[k] * alphak) / Ck_1
            Beta.append(betak)
            Alpha.append(alphak)

        # Append results
        results_iter = pd.Series(name=self.round, dtype=float)
        results_iter['round'] = self.round
        for k in range(K):
            name1 = 'norm_tilde_mu_' + str(k + 1)
            name2 = 'norm_tilde_W_' + str(k + 1)
            name3 = 'tilde_sigma2_' + str(k + 1)
            name4 = 'sigma2_tilde_mu_' + str(k + 1)
            name5 = 'sigma2_tilde_W_' + str(k + 1)
            name6 = 'sigma2_tilde_sigma2_' + str(k + 1)
            results_iter[name1] = np.linalg.norm(tilde_muk[k])
            results_iter[name2] = np.linalg.norm(tilde_Wk[k])
            results_iter[name3] = tilde_Sigma2k[k]
            results_iter[name4] = sigma_til_muk[k]
            results_iter[name5] = sigma_til_Wk[k]
            results_iter[name6] = Beta[k] ** 2 / (((Alpha[k] - 1) ** 2) * (Alpha[k] - 2))

        results_df = results_df.append(results_iter)

        # Update global parameters
        self.tilde_muk = tilde_muk
        self.tilde_Wk = tilde_Wk
        self.tilde_Sigma2k = tilde_Sigma2k
        self.Alpha = Alpha
        self.Beta = Beta
        self.sigma_til_muk = sigma_til_muk
        self.sigma_til_Wk = sigma_til_Wk

        return results_df

    @property
    def global_params(self):
        # Return global parameters
        return self.tilde_muk, self.tilde_Wk, self.tilde_Sigma2k, self.Alpha, self.Beta, self.sigma_til_muk, self.sigma_til_Wk

    def simuTnk(self, view):
        # Sample t_n(k) from posterior
        q = self.n_components
        D_i = self.dim_views

        Wk = matrix_normal.rvs(mean=self.tilde_Wk[view].reshape(D_i[view], q),
                               rowcov=np.eye(D_i[view]),
                               colcov=self.sigma_til_Wk[view] * np.eye(q)).reshape(D_i[view], q)
        muk = multivariate_normal.rvs(mean=self.tilde_muk[view].reshape(D_i[view]),
                                      cov=self.sigma_til_muk[view] * np.eye(D_i[view])).reshape(D_i[view], 1)
        sigma2k = float(invgamma.rvs(a=self.Alpha[view], scale=self.Beta[view]))

        mean = muk
        cov = Wk.dot(Wk.T) + sigma2k * np.eye(D_i[view])

        Tngk = multivariate_normal.rvs(mean=mean.reshape(D_i[view]), cov=cov).reshape(D_i[view], 1)

        return Tngk

    def count_views(self):
        # Count number of centers with each view
        K = self.n_views
        C = self.n_centers

        Tot_C_k = []
        for k in range(K):
            TotCk = 0
            for c in range(C):
                TotCk += self.ViewsC[c][k]
            Tot_C_k.append(TotCk)

        return Tot_C_k

    @staticmethod
    def inv_digamma(y, eps=1e-8, max_iter=100):
        '''Numerical inverse to the digamma function by root finding'''

        if y >= -2.22:
            xold = np.exp(y) + 0.5
        else:
            xold = -1 / (y - digamma(1))

        for _ in range(max_iter):

            xnew = xold - (digamma(xold) - y) / polygamma(1, xold)

            if np.abs(xold - xnew) < eps:
                break

            xold = xnew

        return xnew
